/*jslint node:true, vars:true, bitwise:true, unparam:true */
/*jshint unused:true */



var m = require('mraa'); //IO Library
var app = require('express')(); //Express Library
var server = require('http').Server(app); //Create HTTP instance
var mongodb = require('mongodb');

var io = require('socket.io')(server); //Socket.IO Library

var blinkInterval = 1000; //set default blink interval to 1000 milliseconds (1 second)
var ledState = 1; //set default LED state

var myLed = new m.Gpio(3); //LED hooked up to digital pin 13
var waterPump = new m.Gpio(4);
var toggleLed = "blink";
myLed.dir(m.DIR_OUT); //set the gpio direction to output
var lightSensor = new m.Aio(0);
var tempSensor = new m.Aio(1);
var moistureSensor = new m.Aio(2);

//Read Light Sensor//
function getLux(analogValue) {
  // Values taken from Grove Starter Kit for Arduino table
  var lux;
  var calib = [{reading:0, lux:0},
               {reading:100, lux:0.2},  // guess - not from published table
               {reading:200, lux:1},
               {reading:300, lux:3},
               {reading:400, lux:6},
               {reading:500, lux:10},
               {reading:600, lux:15},
               {reading:700, lux:35},
               {reading:800, lux:80},
               {reading:900, lux:100}];
  var i = 0;
  while (i < calib.length && calib[i].reading < analogValue) {
    i ++;
  }
  if (i > 0) {
    i = i - 1;
  }
  // simple linear interpolation 
  lux =  (calib[i].lux *(calib[i + 1].reading - analogValue) + calib[i + 1].lux * (analogValue - calib[i].reading))/(calib[i + 1].reading - calib[i].reading);
  return lux;
}

var lightReading;

app.get('/', function (req, res) {                  
    res.sendFile(__dirname + '/index.html'); //serve the static html file
}); 

app.get('/simple', function (req, res) {
    console.log("Got a simple request");
    res.json({response: 'Success'});
});
app.get('/temperature', function (req, res) {
    var tempVal = tempSensor.read();
    
    //Calcualte the temp from resistance
    var resistance=((1023-tempVal)*10000/tempVal); //get the resistance of the sensor;
    var temperature=1/(Math.log(resistance/10000)/4275+1/298.15)-273.15;
    var farTemp = temperature *(9/5)+32;
    res.json({response: farTemp});
});
app.get('/light', function (req, res) {
    var lux = getLux(lightSensor.read())
    res.json({response: lux});
});
app.get('/moisture', function (req, res) {
    res.json({response: moistureSensor.read()});
});
app.get('/realtime', function (req, res) {
    var tempVal = tempSensor.read();
    
    //Calcualte the temp from resistance
    var resistance=((1023-tempVal)*10000/tempVal); //get the resistance of the sensor;
    var temperature=1/(Math.log(resistance/10000)/4275+1/298.15)-273.15;
    var farTemp = temperature *(9/5)+32;
    
    var lux = getLux(lightSensor.read())
    
    var moistureVal = moistureSensor.read()
    
    res.json({light: lux, temp: farTemp, moisture: moistureVal})
});
app.get('/ledon', function (req, res) {
    myLed.write(1);
    res.json({led: 'On'}); 
}); 

app.get('/ledoff', function (req, res) {
    myLed.write(0);
    res.json({led: 'Off'});
}); 

app.get('/waterplants', function (req, res) {
    waterPlants();
    res.json({plants: "being watered"});
});

app.get('/wateron', function(req, res) {
    waterPump.write(1);
    res.json({water: "on"});
});

app.get('/wateroff', function(req, res) {
    waterPump.write(0);
    res.json({water: "off"});
});

io.on('connection', function(socket){
    socket.on('changeBlinkInterval', function(data){ //on incoming websocket message...
        toggleLed = "blink";
        blinkInterval = data; //update blink interval
    });
    socket.on('turnOnLED', function(){
        toggleLed = "toggle";
        ledState = 1;
    });
    socket.on('turnOffLED', function(){
        toggleLed = "toggle";
        ledState = 0;
    });
});                                                   


//blink(); //start the blink function

// Standard URI format: mongodb://[dbuser:dbpassword@]host:port/dbname

var uri = 'mongodb://ftscreations:west8246@ds061676.mlab.com:61676/heroku_z3f90vnw';
var readings;

mongodb.MongoClient.connect(uri, function(err, db) {

    if(err) throw err;
    
    readings = db.collection('readings');
    server.listen(3000); //run on port 3000
    console.log("We are running...");
    logValues();
});

function blink(){                                                                               
    if(toggleLed == "toggle"){
        myLed.write(ledState); 
        console.log("toggle LED", ledState);
        setTimeout(blink,1000);
    }
    if(toggleLed == "blink"){
        console.log("blink LED");
        myLed.write(ledState); //write the LED state
        ledState = 1 - ledState; //toggle LED state
        setTimeout(blink,blinkInterval); //recursively toggle pin state with timeout set to blink interval
    }
}

//Initialize vars for logLuxVal
var hourCounter = 0;
var readingIncrement = 300000; //300000 is 5 minutes in milliseconds
var luxReadings = [], tempReadings = [], moistureReadings = [];

var dayCounter = 0;

function logValues(){
    var luxVal = getLux(lightSensor.read());
    var tempVal = tempSensor.read();
    var moistureVal = moistureSensor.read();
    
    hourCounter++;
    
    //Calcualte the temp from resistance
    var resistance=((1023-tempVal)*10000/tempVal); //get the resistance of the sensor;
    var temperature=1/(Math.log(resistance/10000)/4275+1/298.15)-273.15;
    var farTemp = temperature *(9/5)+32;
    
    luxReadings.push(luxVal);
    tempReadings.push(farTemp);
    moistureReadings.push(moistureVal);
    
    if(hourCounter >= (3600000/readingIncrement)){ //3,600,000 is 1 hour in milliseconds
        console.log("Inserting values into DB");
        var values = {
            reading: 
            {light: luxReadings,
             temp: tempReadings,
             moisture: moistureReadings,
             time: Date.now()}
        };
        readings.insert(values, function(err, result) {
            if(err) throw err;
        });
        luxReadings = [], tempReadings = [], moistureReadings = []; //Reset arrays
        hourCounter = 0; //Reset counter for a new hour
        dayCounter++;
    }
    console.log(Date(), " Value: ", temperature);
    if(dayCounter < 8760)
    setTimeout(logValues, readingIncrement);
}
function waterPlants(){
 waterPump.write(1);
    setTimeout(function(){
        waterPump.write(0);
    },5000);
}